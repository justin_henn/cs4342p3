#Justin Henn
#Bisecting k-means using euclidean or manhattan distance

import random
import math
import statistics
from random import randint
from matplotlib import pyplot as plt
import sys

#create data set, list for clusters and centroids, and variable to set manhattan or euclidean distance usage
random.seed(1)
k = 4
num_epochs = 10
num_data = 20
bisect = 2
data_set = [[round(random.uniform(1.0,100.0),1) for i in range (2)] for i in range (num_data)]
list_of_clusters = []
final_centroids = []
use_manhattan = 0

#function to calculate euclidean distance
def euclidean_distance(point, centroid):
    return math.sqrt((point[0] - centroid[0])**2 +(point[1] - centroid[1])**2)

#function to calculate manhattan distance
def manhattan_distance(point, centroid):
    return (abs(point[0] - centroid[0])) + (abs(point[1] - centroid[1]))
    
##function to calculate    
#def compute_cluster(distance, working_cluster, point):
#
#    first_cluster = []
#    second_cluster = []
#    return_clusters = []
#    if distance[0] < distance[1]:
#        second_cluster.append(working_cluster[point])
#    else:
#        first_cluster.append(working_cluster[point])
#    return_clusters.append(first_cluster)
#    return_clusters.append(second_cluster)
#    return return_clusters

#function to move the centroids based on the mean
def move_centroids_mean(cluster_label, num_centroid, centroid):

    totalX = 0
    totalY = 0
    num_nodes = 0
    for i in range(len(cluster_label)):
        totalX += cluster_label[i][0]
        totalY += cluster_label[i][1]
        num_nodes += 1
    if num_nodes > 0:
        return[totalX/num_nodes, totalY/num_nodes]
    else:
        return centroid

#function to calculate the intra cluster distance of each cluster          
def intra_distance_mean():
    distance = []
    for i in range (len(list_of_clusters)):
        total_distance_for_cluster = 0
        for z in range(len(list_of_clusters[i])):
            total_distance_for_cluster += math.sqrt((list_of_clusters[i][z][0] - final_centroids[i][0])**2 + (list_of_clusters[i][z][1] - final_centroids[i][1])**2)
        distance.append(total_distance_for_cluster)
    return distance

#function to calculate the inter cluster distance of the clusters    
def inter_distance_mean():
    distance = []
    for i in range (len(final_centroids)):
        total_distance_for_cluster = []
        z = i
        while z < (len(final_centroids)-1):
            total_distance_for_cluster.append(math.sqrt((final_centroids[i][0] - final_centroids[z+1][0])**2 +(final_centroids[i][1] - final_centroids[z+1][1])**2))
            z += 1
        distance.append(total_distance_for_cluster)
    del distance[len(distance)-1]
    min_distance = sys.float_info.max
    max_distance = -1
    max_first_cluster = 0
    max_second_cluster = 0
    min_first_cluster = 0
    min_second_cluster = 0
    for i in range (len(distance)):
        for z in range (len(distance[i])):
            if distance[i][z] > max_distance:
                max_distance = distance[i][z]
                max_first_cluster = i + 1
                max_second_cluster = z + max_first_cluster + 1
            if distance[i][z] < min_distance:
                min_distance = distance[i][z]
                min_first_cluster = i + 1
                min_second_cluster = z + min_first_cluster + 1
            
    return max_distance, max_first_cluster, max_second_cluster, min_distance, min_first_cluster, min_second_cluster

#function that does k-means on the working cluster
def k_means(centroids, working_cluster):
    temp_cluster_label = []
    first_cluster = []
    second_cluster = []
    proceeding_centroids = []
    while True:
        first_cluster.clear()
        second_cluster.clear()
        for point in range(len(working_cluster)):
            distance = [0]*bisect
            for num_centroid in range(bisect):
                if use_manhattan == 0:
                    distance[num_centroid] = euclidean_distance(working_cluster[point], centroids[num_centroid])
                else:
                    distance[num_centroid] = manhattan_distance(working_cluster[point], centroids[num_centroid])
                    
            if distance[0] < distance[1]:
                first_cluster.append(working_cluster[point])
            else:
                second_cluster.append(working_cluster[point])   
        centroids[0] = move_centroids_mean(first_cluster, num_centroid, centroids[0])
        centroids[1] = move_centroids_mean(second_cluster, num_centroid, centroids[1])
        if proceeding_centroids == centroids:
            break
        else:
            proceeding_centroids = centroids.copy()
    temp_cluster_label=([first_cluster, second_cluster])
    return temp_cluster_label, centroids

#function that randomly picks centroids
def mark_centroids(working_cluster):
    centroids = []
    first_mark = randint(0, len(working_cluster)-1)
    second_mark = first_mark
    for i in range (bisect):
        centroids.append(working_cluster[second_mark])
        while first_mark == second_mark:
            second_mark = randint(0, len(working_cluster)-1)
    return centroids

#function that picks the cluster to work on
def pick_cluster(list_of_clusters):
    which_set = 0
    for i in range(len(list_of_clusters)):
        if len(list_of_clusters[i]) > len(list_of_clusters[which_set]):
            which_set = i
            
    return list_of_clusters[which_set], which_set

#function that runs bisecting k-means
def bisect_kmeans():
    list_of_clusters.append(data_set.copy())
    final_centroids.append(mark_centroids(data_set))
    while len(list_of_clusters) != k:
        clustered_sets = [0]*num_epochs
        centroids = []
        working_cluster, cluster_to_remove = pick_cluster(list_of_clusters)
        del list_of_clusters[cluster_to_remove]
        del final_centroids[cluster_to_remove]
        for i in range (num_epochs):
            centroids.append(mark_centroids(working_cluster))
            clustered_sets[i], centroids[i] = k_means(centroids[i], working_cluster)
        SSE_total = []
        for i in range(num_epochs):
            SSE_1 = 0
            SSE_2 = 0
            for x in range(len(clustered_sets[i][0])):
              SSE_1 += (clustered_sets[i][0][x][0] - centroids[i][0][0])**2 +(clustered_sets[i][0][x][1] - centroids[i][0][1])**2
            for z in range(len(clustered_sets[i][1])):
              SSE_2 += (clustered_sets[i][1][z][1] - centroids[i][1][0])**2 +(clustered_sets[i][1][z][1] - centroids[i][1][1])**2
            SSE_total.append(SSE_1)
        which_cluster = 0
        lowest_SSE = SSE_total[0]
        for i in range(len(clustered_sets)):
            if SSE_total[i] < lowest_SSE:
                which_cluster = i
                lowest_SSE = SSE_total[i]
        list_of_clusters.append(clustered_sets[which_cluster][0])
        list_of_clusters.append(clustered_sets[which_cluster][1])
        final_centroids.append(centroids[which_cluster][0])
        final_centroids.append(centroids[which_cluster][1])
                       

#run bisecting k-means
bisect_kmeans()

#calculate intra cluster distance
intra_list = intra_distance_mean()

#print data
for i in range(len(intra_list)):
    print("cluster number: %d" % (i+1))
    print("intra cluster distance: %.3f" % (intra_list[i]))
inter_list = inter_distance_mean()
print("Max inter cluster distance is cluster %d to %d with distance %.3f" % (inter_list[1], inter_list[2], inter_list[0]))
print("Min inter cluster distance is cluster %d to %d with distance %.3f" % (inter_list[4], inter_list[5], inter_list[3]))  

#plot data  
axes = plt.gca()
for i in range(len(list_of_clusters)):
    x, y = zip(*list_of_clusters[i])
    plt.scatter(x,y)
x, y = zip(*final_centroids)
plt.scatter(x, y, color = "black")
